// Задание:
// Создайте страницу с кнопкой, 
// при нажатии по кнопке на странице создается div с произвольнім тестом.
// После создания 10 дивов все они должны удалится

const windowInnerHeight = window.innerHeight;
const textArea = document.querySelector('#text-area');
const btn = document.querySelector('#btn');

textArea.style.height = `${windowInnerHeight /  2 - 100}px`;
btn.onclick = addDeleteDivs();

function addDeleteDivs() {
    let counter = 0;
    function addDivs() {
        let div;
        if(counter < 10){
            div = document.createElement('div');
            div.className = 'our-text';
            div.innerHTML = `Произвольный  текст ${counter + 1}`;
            textArea.append(div);
            counter++
        } else {
            let ourText = document.querySelectorAll('.our-text');
            ourText.forEach(elem => elem.remove());
            counter = 0;
        }
    }
    return addDivs;
}
